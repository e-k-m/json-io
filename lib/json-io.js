var fs = require('fs')

exports.read = function read (path, callback) {
  fs.readFile(path, function (err, buffer) {   
    if (err) return callback(err)
    return callback(err, JSON.parse(buffer.toString()))
  })
}

exports.readSync = function readSync (path) {
  var buffer = fs.readFileSync(path)
  return JSON.parse(buffer.toString())
}

exports.write = function write (path, data, callback) {
  return fs.writeFile(path, JSON.stringify(data), callback)
}

exports.writeSync = function writeSync (path, data) {
  return fs.writeFileSync(path, JSON.stringify(data))
}