var test = require('tap').test;
var config = require('../lib/json-io.js')
var path = require('path')
var fs = require('fs')

var p1 = path.join(__dirname,'test.json')
var p2 = path.join(__dirname,'_.json')
var o1 = {a: 1, b: '2', c: {d: 1}}

test("read tests", function (t) {
  fs.writeFileSync(p1, JSON.stringify(o1))
  t.deepEqual(config.readSync(p1), o1, 'reading sync should read correctly')

  config.read(p1, function (err, doc) {
    t.deepEqual(doc, o1, 'reading async should read correctly')
    t.end()    
  })

  try {
    var conf = config.readSync(p2)    
  } catch (err) {
    t.type(err, "Error", "reading sync of non-existing file should throw error")
  }

  /*config.read(p2, function (err, doc) {
    t.type(err, "Error", "reading async of non-existing file should throw error")
    t.end()
  })*/

})

test("write tests", function (t) {
  config.writeSync(p1, o1)
  var f = JSON.parse(fs.readFileSync(p1).toString())
  t.deepEqual(f, o1, 'write sync should write correctly')

  config.write(p1, o1, function (err) {
    t.deepEqual(f, o1, 'write async should write correctly')
    t.end()
    fs.unlinkSync(p1)
  })
})

